const express = require('express');
const logger = require('morgan');
const port = 5200;
  
const app = express();

// app.use(logger('tiny'));
const responseFormat = `:method :url :status :response-time ms - :res[content-length]`;
app.use(logger(responseFormat,{
    skip:(req,res)=>res.statusCode >= 400,
    stream:{write:(message) => console.log(`success || ${message.trim()}`)}
}));

app.use(logger(responseFormat,{
    skip:(req,res)=>res.statusCode < 400,
    stream:{write:(message) => console.error(`error || ${message.trim()}`)}
}));

app.get('/', (req, res) => {
  res.send('<h1>Front Page</h1>');
});

app.get('/success', (req, res) => {
    res.json({
        "success":true,
        "message":"done"
    });
  });
  
  app.get('/error', (req, res) => {
    res.json({
        "success":false,
        "message":"error"
    }).status(400);
  });
app.listen(port, () => {
  console.log(`Started at ${port}`);
});