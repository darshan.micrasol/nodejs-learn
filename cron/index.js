const cron = require('node-cron');

cron.schedule('* * * * *', () => {
    console.log('Running a task every minute');
});


cron.schedule('7 18 * * *', () => {
    console.log('Running a task at 18:07 every day');
});
  
// second
// minute
// hour
// day of month
// month
// day of week	